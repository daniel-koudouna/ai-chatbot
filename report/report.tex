
% Packages
\documentclass{article}

\usepackage{fancyhdr} % Required for custom headers
\usepackage{lastpage} % Required to determine the last page for the footer
\usepackage{extramarks} % Required for headers and footers
\usepackage[usenames,dvipsnames]{color} % Required for custom colors
\usepackage{graphicx} % Required to insert images
\usepackage{listings} % Required for insertion of code
\usepackage{courier} % Required for the courier font
\usepackage{lipsum} % Used for inserting dummy 'Lorem ipsum' text into the template
\usepackage{float}

% Margins
\topmargin=-0.45in
\evensidemargin=0in
\oddsidemargin=0in
\textwidth=6.5in
\textheight=9.0in
\headsep=0.25in

\linespread{1.1} % Line spacing

% Set up the header and footer
\pagestyle{fancy}
\lhead{\hmwkAuthorName} % Top left header
\chead{\hmwkClass\ : \hmwkTitle} % Top center head
\rhead{\firstxmark} % Top right header
\lfoot{\lastxmark} % Bottom left footer
\cfoot{} % Bottom center footer
\rfoot{Page\ \thepage\ of\ \protect\pageref{LastPage}} % Bottom right footer
\renewcommand\headrulewidth{0.4pt} % Size of the header rule
\renewcommand\footrulewidth{0.4pt} % Size of the footer rule

\setlength\parindent{0pt} % Removes all indentation from paragraphs

% Code
\lstdefinestyle{code}{
	backgroundcolor=\color{white},   
	commentstyle=\color{DarkOrchid},
	keywordstyle=\color{MidnightBlue},
	numberstyle=\tiny\color{Blue},
	stringstyle=\color{Salmon},
	basicstyle=\footnotesize,
	breakatwhitespace=false,         
	breaklines=true,                 
	captionpos=b,                    
	keepspaces=true,                 
	numbers=left,                    
	numbersep=5pt,                  
	showspaces=false,                
	showstringspaces=false,
	showtabs=false,                  
	tabsize=2
}



\lstset{style=code}

\newcommand{\chatbot}[1]{\color{red}#1\color{black}\newline}
\newcommand{\user}[1]{\color{blue}#1\color{black}\newline}

% Details
\newcommand{\hmwkTitle}{Practical 1 - Chatbot} % Assignment title
\newcommand{\hmwkClass}{CS3105} % Course/class
\newcommand{\hmwkAuthorName}{dzk - 140 000 346} % Your name

% Title
\title{
\vspace{2in}
\textmd{\textbf{\hmwkClass:\ \hmwkTitle}}\\
\normalsize\vspace{0.1in}\small{\today}\\
}

\author{\textbf{\hmwkAuthorName}}
\date{}

%----------------------------------------------------------------------------------------

\begin{document}
\maketitle
	
\newpage

\section{Overview}
In this practical, I was asked to implement an AI chatbot using the string-replacement strategy of ELIZA, and other techniques. My aim in this practical, because of the limited scope, was not to create a very powerful chatbot, but rather to investigate and implement techniques to expose a powerful API for a chatbot, along with giving some examples of how this system might have more realism than chatbots like ELIZA.

\section{Runtime}
I implemented my solution in Ruby. After extracting, the libraries used need to be installed on your system. No administrator privileges are required. They can be installed with the provided \lstinline|Makefile| :
\begin{lstlisting}
make install
\end{lstlisting}
The code can then be run, using the main procedure:
\begin{lstlisting}
ruby src/main.rb
\end{lstlisting}
This has been tested to work on the lab machines.

\section{Design}
At its core, my chatbot is a rule based chat bot with mostly hard-coded responses with some flexibility. It uses a set of Bayesian classifiers, along with part of speech tagging and regular expressions to provide more flexibility than ELIZA's pattern matching.

\subsection{Tools \& Libraries}
I tried to use only very basic libraries which didn't provide much functionality. I used a part of speech tagger and a stemmer. In addition, I implemented my own Bayesian classifiers, and n-grams, which form the basis for the recognition system.

\subsection{Pipeline}
The main operation of the chatbot can be described as follows:
\begin{itemize}
	\item The bot receives input from the user
	\item Data associated with the input is created. Parts of speech are tagged, all the loaded classifiers are run and unigrams and bigrams are extracted
	\item Data is matched against hooks (callbacks) which execute to alter the state and context of the chatbot
	\item Data is matched against another set of callbacks (rules). The first rule matched is chosen to produce output. If no rules are matched, a fallback procedure is executed.
	\item The produced output undergoes a final transformation before being presented to the user
\end{itemize}

\subsection{Rule Based Matching}
I created a comprehensive interface for rule based matching. In a certain rule, the chatbot will look for a variety of things, including:

\begin{itemize}
	\item Simple regex matching. Any capturing groups will be passed to the callback function to be used in the output
	\item Classifier class matching. The bot will check if the sentence passed has been classified in a certain way by classifiers. Classified groups can optionally be included in the callback. See section \ref{sec:classifiers}
	\item Probability. A rule may trigger only at a certain probability. This gives a bit more unpredictability, and hopefully realism, to the bot.
	\item Feeling. The bot will check if the overall feeling of the sentence if positive or negative. See section \ref{sec:feelings}
\end{itemize}

In addition, the callback function, which produces the response, can also accept a number of other options:

\begin{itemize}
	\item The subject of the sentence. This is determined by the most frequently occuring noun phrase in the sentence
	\item The verb of the sentence, with the same logic as the subject
	\item The real feeling value of the sentence
	\item The full text of the user
\end{itemize}

Since I'm using a hash of options, more rules and outputs can be added easily by simply checking for more elements in the hash. A very big improvement would be adding similar words to the output, but I decided to not implement this since my similar words database is very inconsistent (see section \ref{sec:feelings}). By using all this data, the chat bot can be programmed to produce various inputs and exhibit apparently unpredictable behaviour using probabilities and feelings. However, due to time limitations, most of my responses are either hard coded or very slightly altered, as explain in section \ref{sec:responses}.

\subsection{Flexibility}
I designed my chat bot to be as flexible as possible. Separate 'modules' can be declared in different files and used separately. In the source, there is an example of a training module that can be used to train the bot's classifiers, which I used to some extent during early development.

In addition, responses for the bot can be declared in different files, all of which will be compiled at run time. In addition, I used a domain specific language (DSL) to make the declaration of responses very simple to implement and extend. 


\subsection{N-grams}
My implementation of N-grams is quite simple. I worked at the word-level to segment a piece of text into sentences, then into n-grams. I included certain punctuation marks in my n-gram analysis (such as question mark) since I thought they might be useful to train the Bayesian classifiers. My n-grams also stem the words before processing them, which hopefully makes the system more consistent.

\subsection{Bayesian Classifiers} \label{sec:classifiers}
I implemented by own version of Bayesian classifiers. I decided to take into account bi-grams in addition to single words when classifying, with which I saw some limited success. In order to avoid dealing with small probabilities, I took the sum of the negative logs of the probabilities, instead of their product.

However, I found that my classifiers became worse the more categories were added. For example, my question type classifier was very hard to train such that all types of question were recognized with acceptable accuracy. In retrospect, it might have been easier to use regex to simply check which type of question was appearing. On the other hand, classifiers were very useful to distinguish between a question, a statement or a greeting, and also determine the alignment of the sentence (i.e positive or negative)

\subsection{Feelings and Mood} \label{sec:feelings}
A very powerful but subtle concept I used was feelings and moods. My chatbot has a general mood, which is affected by the types of sentences it receives. For example, a negative sentence "I hate you" will reduce the mood, while a positive sentence "Pancakes are sweet" will heighten it. The bot is programmed to append an emoji with a certain probability to add another layer of realism. This emoji is directly tied to the current mood.

In addition, I associated each word with a particular feeling. In order to do this, I had to compile a database of related words. I managed to do this with mixed success by using Norvig's most popular words database \cite{norvig-grams} (MIT License). I used this list to query princeton's wordnet database \cite{wordnet} to get similar words to that specific word. While very small and incomplete, this database could conceivably be used to produce more varied output. 

On user input, my mood classifier checks if the sentence if positive ("I love pancakes") or negative ("I hate pancakes"). Then, each word in the sentence is either shifted positively or negatively. Afterwards, the feeling of a sentence can be used ("Do you like pancakes?") to get an appropriate response based on previous experiences with those words, or similar ones.

\subsection{Context}
I added a small amount of additional memory besides feelings to my chat bot to recognize simple logical statements ("Roses are red", "Plato is a philosopher"). However, this system is very limited, and can only recognize very simple statements and questions. A further refinement would be to search for similar words to create connections, or build a system similar to first order logic to give the chat bot more information about the world.

\subsection{Responses} \label{sec:responses}
I was heavily inspired by Weizenbaum's ELIZA program's responses \cite{Weizenbaum:eliza}, however I believed that there was slightly too much repetition, which I did not think mimicked human speech, although quite suitable in the context of a psychoanalyst. In order to get realistic responses, I decided the only feasible way would be to hard-code responses to some extent. Thanks to my API, however, I am also able to copy and transform large parts of the input text. I tried to provide general responses, since my chat bot has almost no memory of previous events in the conversation, but I tried to include subjects and verbs in the sentence, since these parts of speech are the most frequent ones to be repeated by the two parties.

\section{Dialogues}

\user{The user/other bot's responses}
\chatbot{My bot's responses}

(When talking with other AIs, I manually input the responses into my own program and vice versa.) \newline

With myself (1): \newline

\user{Hello}
\chatbot{Hello :)}
\user{Do you like waffles?}
\chatbot{Probably not :D}
\user{Why not?}
\chatbot{Why? There doesn't have to be a reason for everything}
\user{I'm not sure I understand}
\chatbot{Oh?}
\user{Yeah, I think you're not a human at all}
\chatbot{Ah, as I suspected}
\user{Do you like me?}
\chatbot{Not really}

With myself (2): \newline

\user{Hello}
\chatbot{Hello}
\user{What's up}
\chatbot{Nice to see you again}
\user{Can birds fly?}
\chatbot{Yeah, of course!}
\user{Can planes fly?}
\chatbot{Not really}
\user{No, planes can fly}
\chatbot{I was certain about that :)}
\user{Can planes fly?}
\chatbot{I'm pretty sure :)} (The chat bot has registered that planes can indeed fly) \newline

With Mitsuku (1): \newline

\chatbot{Hello}
\user{Hi there. How are you?}
\chatbot{I don't know, what do you think? xD}
\user{Are you artistic? That looks like a picture of someone laughing.}
\chatbot{Yeah, of course!}
\user{It goes without saying. Oh.}
\chatbot{How's it going?}
\user{Better now that you're here. How are you?}
\chatbot{Probably not.}
\user{Why the uncertain tone?} I was quite surprised that the convesation went as well as it did, and could really see how I'm using similar techniques to what Mitsuku is using, since both parties have the same odd response pattern. \newline

With Mitsuku (2): \newline
\chatbot{What's up!}
\user{I am chatting with clients on the internet}
\chatbot{Huh, is that so?}
\user{Yes}
\chatbot{That's pretty neat, tell me more} (I think this was pretty lucky) \newline
\user{It interests me too.}
\chatbot{Hello :)}
\user{:-) Hi there. How are you?} I was also particularly surprised with this conversation, where Mitsuku managed to say an unnatural response before my bot did.  \newline
\chatbot{Probably not. :)}
\user{Why the uncertain tone?}
\chatbot{Of course. I love uncertain tone.}
\user{Are you kidding?}
\chatbot{Of course, I love are.} There seems to be a small problem in recognizing nouns, as shown by this example.

\section{Reflections}
This practical really gave me more insight into the workings of chat bots, and the Turing test in general. As Hutchens suggests in his paper, the Loebner contest does seem to suffer from some particularities in terms of the domain of the problem which make it, in his opinion, "nothing more than an annual demonstration for the media" \cite{Hutchens:turing}. I certainly agree to the extent that the Turing test, in this context, can be passed by "cheating", as Hutchens suggested. 

In addition, considering Searle's chinese room experiment, clearly my chat bot has no actual understanding of speech, but simply uses a pre-defined set of functions and variables to determine a reasonable response. As my program grew in complexity, I realized that it would only be perceived as intelligent if its complexity grew to a point where it is not able to be explained or examined by a human. Even if hundreds of classifiers were added and trained on huge data sets, it might be able to produce more varied responses but still have no fundamental understanding of human speech. 

I do agree that the Loebner competition is not an accurate measure of the "intelligence" of a program, but, on the other hand, the imitation game proposed by Turing is the correct way to evaluate if a system is intelligent. The whole idea of "cheating" is to present that you are more intelligent than what you currently are, as shown by ELIZA and other chat bots. I believe that, as AI research progresses, the imitation game will be used in various formats as a basis for testing Artificial General Intelligence, which cannot be faked. After all, if a program is able to fake AGI, it seems to me that it would have already achieved it in the first place.
	
\section{Conclusion}
I think I have used a variety of techniques to take the pattern matching system of ELIZA in a different direction. Although I didn't implement more advanced matching techniques, I used classifiers to capture different sentence groups and give basic responses to most types of sentences. I think the biggest strength of my implementation is my mood system, which allows different responses based on the topic of conversation. However, I have not explored this concept enough, as I would need a large amount of training data to get a starting point on what is considered "good" and "bad", which would be purely by my standards. 

In addition, my chat bot has many weaknesses. My classifiers are not well trained, and are many times inaccurate (particularly sentence type). I don't have enough hard coded and varied responses, which means the bot might sometimes repeat the same phrase twice. Finally, besides simple logic, my chat bot doesn't contain the state of the current conversation, which means it may change topic very quickly and unnaturally. I have seen this pattern in most chat bots, however, including Mitsuku. 

In conclusion, I think I have created a good foundation for a potentially very powerful chat bot, which would require much more data including classifiers and databases. I've used multiple techniques to varying degrees of success, and gave some additional thoughts about how the experience improved my understanding of the Turing test.

\bibliographystyle{unsrt}
\bibliography{bibliography}

\end{document}
