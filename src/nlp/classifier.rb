require_relative './ngram.rb'
require 'fast_stemmer'

# Implementation of a bayesian classifier
class Classifier
  def initialize
    @categories = []
    @training_data = {}
    @training_counts = {}
    @category_counts = {}
    @training_total = 0
    @total_grams = 0
  end

  # Load a classifier from a file
  def self.load(path)
   Marshal.load(File.read path) 
  end

  # Save the classifier to a file
  def save(path)
    File.open(path, "w") {|f| f.write( Marshal.dump self) }
  end

  # Train the classifier on a specific category
  def train(text, category)
    # Create the category if it doesn't exist yet
    if (!@categories.include? category)
      @categories << category
      @category_counts[category] = 0
      @training_data[category] = {}
      @training_counts[category] = 0
    end

    # Split the text into unigrams and bigrams, and add them to the training data of
    # the trained category
    grams = Ngrams.of(text,[1,2]).each do |gram|
      @training_data[category][gram] = 0 unless @training_data[category].has_key? gram
      @training_data[category][gram] += 1
      @category_counts[category] += 1
      @total_grams += 1
    end

    # Increment the total training count
    @training_total += 1
    @training_counts[category] += 1
  end

  # Get the category scores of all categories
  def get_category_scores(text)
    score = {}
    grams = Ngrams.of(text,[1,2])

    @categories.each do |category|
      score[category] = 0
      grams_in_category = @category_counts[category].to_f

      # For each gram, multiply (add logs) by the probability of that gram appearing
      # in that category
      grams.each do |gram|
        frequency = @training_data[category][gram] || 0.1
        score[category] += -Math.log( frequency / grams_in_category )
      end

      # Finally multiply by the overall probability of a category
      training_frequency = @training_counts[category] || 0.1
      score[category] += -Math.log( training_frequency / @training_total.to_f )
      
    end

    score
  end

  # Get the category scores of all classes, and choose the highest one
  def classify_with_score(text)
    if (@categories.empty?)
      return [:empty, 0]
    end
    get_category_scores(text).sort_by { |a| a[1] }.first
  end

  # Classify a piece of text
  def classify(text)
    classify_with_score(text)[0]
  end
  
end

