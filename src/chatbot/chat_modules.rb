# Class which serves as a DSL to register different
# chat bot behaviours
class ChatModules
  @@modules = {}
  def self.register(name, &block)
    @@modules[name] = block
  end

  def self.[](name)
    @@modules[name]
  end

  def self.include?(name)
    @@modules.include? name
  end
  
  
end
