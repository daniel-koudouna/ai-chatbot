# Class which serves as a DSL to register chat bot
# responses for given response tags
class ChatResponses
  @@responses = {}

  def self.add(tag, response)
    @@responses[tag] = [] unless @@responses.include? tag
    @@responses[tag] << response
  end

  # Get an appropriate response given a tag and a list of words
  # The list is used to replace special characters ($$) in the response
  # If no words are given, a response without special characters is chosen
  def self.get(tag, *args)
    return self.get(:filler) unless @@responses.has_key? tag
    args = args.compact
    output = nil
    while (true)
      output = @@responses[tag].sample.dup
      next if (output[/\$\$/] != nil && (args == nil || args == []))
      cycle = args.cycle
      while (output[/\$\$/] != nil)
        output.gsub! /\$\$/, cycle.next
      end 
      break
    end
    return self.get(:filler) if (output == nil)
   output.capitalize
  end

  def self.register(&block)
    self.instance_eval &block
  end

  # DSL construct to allow more syntactic sugar
  def self.method_missing(name, *args)
    self.add(name, args[0])
  end
  
end
