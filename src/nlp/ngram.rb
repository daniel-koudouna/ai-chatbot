require 'fast_stemmer'

# Class to split a sentence into ngrams
class Ngrams

  # Returns the n-grams of a sentence for
  # all numbers passed, as an array
  def self.of(input, nums)
    result = []
    nums.each do |n|
      result << grams(input,n)
    end

    result.flatten
  end

  # Returns the n-grams of a sentence
  def self.grams(input, n)
    # Remove any non-alphanumeric characters, but
    # keep sentence splitters
    # Split into sentences and process each
    # sentence individually
    # Stem each word to find similarities
    input 
    .downcase
    .gsub(/\?/, ' ? ')
    .gsub(/'s/, " 's ")
    .gsub(/[^a-z0-9\s\,\.;\?\!\']/i,'')
    .split(/\,|\.|;/)
    .map { |sentence| sentence
      .split(' ')
      .map{|w| w.stem}
      .each_cons(n)
      .to_a
      .map{|w| w.join(' ') }
      .flatten }
    .flatten
  end
  
end
