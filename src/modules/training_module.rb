ChatModules.register "Training module" do
  
    on regex: />>new\s(.*)/ do |name|
      classifier = Classifier.new
      @classifiers[@classifier_path + "/new-" + name +".dat"] = classifier
      @current_classifier = classifier
      "Made new classifier"
    end

    on regex: />>show/ do
      @classifiers.keys.each do |key|
        puts key
      end
    end
       
    on regex: />>use (.*)/ do |name|
      if (@classifiers.has_key? name)
        @current_classifier = @classifiers[name]
        "Ok, I switched to using that classifier"
      else
        "I don't have that classifier"
      end
    end

    on regex: />>yes/ do
      @current_classifier.train(@last_input,@last_guess)
      "Got it, " + @last_input + " is " + @last_guess.to_s + " :D"
    end

    on regex: />>save/ do
      @classifiers.each do |path, classifier|
        File.open(path, "w") { |f| f.write(Marshal.dump classifier) }
      end
     "Saved ;)" 
    end
    
    on regex: />>no, (.*)/ do |input|
      word = input.strip.downcase.to_sym
      @current_classifier.train(@last_input, word)
      "Got it, " + @last_input + " is " + input.strip + " :P"
    end

    on regex: />>no/ do
      "What is it then? (Use >>no, CATEGORY_NAME)"
    end

    on regex: /(.*)/ do |input|
      @last_input = input
      @last_guess = @current_classifier.classify(@last_input)
      "I know, that's a " + @last_guess.to_s + ", isn't it?"
    end
     
    if (@classifiers.empty?)
      puts "making new classifier"
      respond_to ">>new Ham"
    end
 
end
