require 'engtagger'

require_relative './chat_modules.rb'
require_relative './chat_responses.rb'

CLASSIFIERS_PATH = File.dirname(__FILE__) + "/../../classifiers"
RESPONSES_DIR = File.dirname(__FILE__) + "/../responses"
MODULES_DIR = File.dirname(__FILE__) + "/../modules"
NLP_DIR = File.dirname(__FILE__) + "/../nlp"

DATA_DIR = File.dirname(__FILE__) + "/../../data/"

FEELINGS_PATH = DATA_DIR + "feelings.dat"
SIMILAR_WORDS_PATH = DATA_DIR + "words.dat"
CONTEXT_PATH = DATA_DIR + "context.dat"

Dir[RESPONSES_DIR + "/*.rb"].each &method(:require)
Dir[MODULES_DIR + "/*.rb"].each &method(:require)
Dir[NLP_DIR + "/*.rb"].each &method(:require)

class ChatBot

  DEBUG = false
  
  attr_accessor :map, :hooks, :mood, :dict
  attr_accessor :tagger

  attr_accessor :classifiers, :current_classifier
  attr_accessor :classifier_path
  
  attr_accessor :last_input, :last_guess

  attr_accessor :feelings
  
  # Constructor
  def initialize(module_name)
    log "Loading Cha..Human"
    @hooks = Hash.new
    @map = Hash.new
    @tagger = EngTagger.new
    @classifiers = Hash.new
    @mood = 100
    init_classifiers CLASSIFIERS_PATH
    init_feelings FEELINGS_PATH 
    init_words SIMILAR_WORDS_PATH
    init_context CONTEXT_PATH
    
    if (module_name != nil && ChatModules.include?(module_name))
      instance_eval &ChatModules[module_name]
    end
  end

  # Load the feelings database
  def init_feelings(path)
    if (File.exist?(path))
      @feelings = Marshal.load( File.read(path) )
    else
      @feelings = {}
    end
    log "Loaded #{@feelings.size} known feelings"
  end

  # Load the words database
  def init_words(path)
    if (File.exist?(path))
      @words = Marshal.load (File.read(path))
    else
      @words = {}
    end
    log "Loaded #{@words.size} known words in the dictionary"
  end

  # Load all the classifiers
  def init_classifiers(path)
    @classifier_path = path
    log "Loading classifiers from #{path}"
    Dir.glob(path + "/*.dat") do |c|
      @classifiers[c] = Marshal.load(File.read(c))
      if (@current_classifier == nil)
        @current_classifier = @classifiers[c]
      end
    end
    log "Loaded #{@classifiers.size} classifiers"
  end

  # Init context
  def init_context(path)
    if (File.exist?(path))
      @context = Marshal.load (File.read(path))
    else
      @context = {}
    end
    log "Loaded #{@context.size} contexts" 
  end

  # Logging function
  def log(string)
    puts "## #{string}" if DEBUG
  end
  
  # Change the general mood of the chat bot
  def add_mood(m)
    @mood += m
    # Clamp between 0 and 100
    @mood = [0,@mood,100].sort[1]
  end

  # Get the synonyms of a particular word from the dictionary
  def get_related(word)
    related = @words[word]
    related ||= []
    related
  end

  # Get all words in the particular phrase along with all the
  # synonyms of these words
  def get_word_cluster(phrase)
    if (phrase == nil)
      return []
    end
    phrase.split(' ')
    .map {|w| [w] +  get_related(w).compact.flatten}
    .compact.flatten
  end

  # Change the mood of particular words in the feelings database by
  # a certain score 
  def add_context(words, score)
    words.each do |w|
      if (!(@feelings.has_key? w))
        @feelings[w] = 50
      end
      @feelings[w] += score
      @feelings[w] = [0,@feelings[w],100].sort[1]
    end
  end

  # Fallback procedure used as a last resort when no matching rules are found
  def fallback_on(inputs)
    "I didn't quite catch that"
  end

  # Save the feelings database which was modified during program execution
  def save
    File.open(FEELINGS_PATH,"w") {|f| f.write Marshal.dump(@feelings) }
    File.open(CONTEXT_PATH, "w") {|f| f.write Marshal.dump(@context) }
  end

  # Register a hook to be executed at each input string
  def hook(options, &block)
    @hooks[options] = block
  end

  # Register a rule to be executed if the options match the input string
  def on(options, &block)
    @map[options] = block
  end
 
  # Returns true or false if the current input text matches the options
  # of a registered callback function
  def accepts(inputs, options, operator=:and)
    accept = set_initial_value operator
    results = []
    if (options[:regex] != nil)
      results << (options[:regex].match(inputs[:text]) != nil)
    end
    if (options[:class] != nil)
      if (options[:class].class == Array)
        options[:class].each { |c| results << inputs[:classes].include?(c) }
      else
        results << inputs[:classes].include?(options[:class])
      end
    end

    if (options[:probability] != nil)
      results << (rand(100) < options[:probability]*100)
    end
    
    if (options[:and] != nil)
      results << accepts(inputs, options[:and], :and)
    end
    if (options[:or] != nil)
      results << accepts(inputs, options[:or], :or)
    end

    if (options[:feeling] != nil)
      feels = inputs[:text].split(" ").select{|w|
        @feelings.has_key? w
      }.map{
        |w| @feelings[w]
      }

      average_feeling = 50
      
      if feels.size != 0
         average_feeling = feels.reduce(:+)/(feels.size*1.0)
      end

      if (options[:feeling] == :positive)
        results << (average_feeling >= 50)
      else
        results << (average_feeling < 50)
      end
    end
    
    return get_result(accept, results, operator)
  end
  
  # Collect the options to be passed as parameters to the callback
  # function, by considering the options passed in when registering it
  def get_response_options(inputs, options)
    outputs = {}

    # Include matching regex groups
    if (options[:regex] != nil)
      matches = options[:regex].match(inputs[:text]).captures
      if (matches.size == 1)
        matches = matches[0]
      end
      outputs[:regex] = matches
      outputs.delete :regex if matches == []
    end

    if (options.has_key? :include)
      includes = options[:include]
      # Include the raw text
      outputs[:text] = inputs[:text] if includes.include? :text
      # Include the tagged text
      outputs[:tags] = inputs[:tags] if includes.include? :tags
      # Include the classes returned by all classifiers
      outputs[:classes] = inputs[:classes] if includes.include? :classes
      # Include the most commonly occuring noun phrase
      if (includes.include? :subject)
        nouns = @tagger.get_noun_phrases inputs[:tags]
        if (nouns == nil || nouns.first == nil)
          outputs[:subject] = nil
        else
          outputs[:subject] = nouns.first[0]
        end
      end
      # Include the most commonly occuring verb phrase
      if (includes.include? :verb)
       verbs = @tagger.get_verbs inputs[:tags]
       if (verbs.first == nil)
         outputs[:verb] = nil
       else
         outputs[:verb] = verbs.first[0]
       end
      end
      # Include the feeling
      if (includes.include? :feeling)
       feels = inputs[:text].split(" ").select{|w|
         @feelings.has_key? wi
       }.map{
         |w| @feelings[w]
       }
       if (feels.size == 0)
         outputs[:feeling] = 50
       else  
         outputs[:feeling] = feels.reduce(:+)/(feels.size*1.0)
       end
     end
    end
    
    # Simplify parameter passing for simple conditions
    if (outputs.size == 1)
      result = outputs.values.flatten
      if (result.size == 1)
        return result[0]
      else
        return result
      end
    end
    outputs
  end

  # Respond to the user's input. The main operation of the chat bot
  def respond_to(input)
    # First, collect the necessary data
    tags = @tagger.add_tags(input)
    classes = []
    @classifiers.each {|path, c| classes << c.classify(input)}
    log "#{classes}"
    grams = Ngrams.of(input,[1,2,3])

    inputs = {text: input, tags: tags, classes: classes, grams: grams}

    # First, execute any hooks that match the inputs
    @hooks.each do |options, response|
      if (accepts(inputs,options))
        response.call (get_response_options(inputs,options))
      end
    end
    
    # Next, Search each of the registered callback functions for a match on the inputs.
    # If found, execute the first callback that matches to produce output.
    @map.each do |options, response|
      if (accepts(inputs,options))
          output = response.call (get_response_options(inputs,options))
          return transform output
      end
    end

    # If there are no matches, use a fallback procedure.
    return fallback_on inputs
  end

  # Set initial value of a clause
  def set_initial_value(operator)
    return true if operator == :and
    return false if operator == :or
    return false
  end

  # Get the final result of a clause
  def get_result(value, results, operator)
    if operator == :and
      results.each {|b| value &= b }
    elsif operator == :or
      results.each {|b| value ||= b }
    end
    value
  end
 
  # Transform the final output before presenting it to the user
  def transform(output)
    output += get_mood_emoji if (rand(100) < 40)
    output
  end

  # Get an emoji based on the current mood
  def get_mood_emoji
    if (@mood > 90)
      return [" :D", " :)", " xD"].sample
    elsif (@mood > 50)
      return [" :)", " :P", " :O"].sample
    elsif (@mood > 20)
      return [" :P", " :|", " :O"].sample
    else
      return [" :\\", " :|", " :("].sample
    end
  end
  
end
