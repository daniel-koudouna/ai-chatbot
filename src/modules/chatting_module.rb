ChatModules.register "Chatting module" do

  hook class: :positive, include: [:text] do |text|
    add_mood(5)
    add_context(get_word_cluster(text),5)
  end

  hook class: :negative, include: [:text] do |text|
    add_mood(-10)
    add_context(get_word_cluster(text),-10)
  end

  hook regex: /(\w+)\s(is|are|can|has)\s(.*[^\?]$)/ do |regex|
    a = regex[0]
    b = regex[2]
    @context[b] = [] if (!@context.has_key? b)
    @context[b] << a
    log "Adding context (#{a}) -> (#{b})"
  end
  
  on regex: /^\s*$/ do
    ["Are you there?", "Hello?"].sample
  end
  
  on regex: /don\'t\sknow/ do
    ChatResponses.get(:echo)
  end
  
  on class: :question, regex: /^are\syou/, include: [:subject] do |subject|
    ChatResponses.get(:boolean_answer,subject)
  end

  on regex: /do\syou\slike\s(.*)\?/, feeling: :positive do |regex|
    ChatResponses.get(:boolean_yes, regex)
  end

  on regex: /do\syou\slike\s(.*)\?/, feeling: :negative do |regex|
    ChatResponses.get(:boolean_no, regex)
  end
    
  on class: [:question, :why], include: [:subject]  do 
    ChatResponses.get(:why_answer)
  end

  on class: [:question, :how], include: [:subject] do
    ChatResponses.get(:how_answer)
  end

  on class: [:question, :where], include: [:subject] do
    ChatResponses.get(:where_answer)
  end

  on class: [:question, :who], include: [:subject]  do |subject|
    ChatResponses.get(:who_answer, subject)
  end
  
  on class: [:question, :what], include: [:subject] do |subject|
      ChatResponses.get(:what_answer, subject)
  end

  on class: :negative_response, include: [:subject] do |subject|
    ChatResponses.get(:negative_response, subject)
  end

  on class: :positive_response, include: [:subject] do |subject|
    ChatResponses.get(:positive_response, subject)
  end

  on class: :statement, probability: 0.7, include: [:subject] do |subject|
    ChatResponses.get(:prompt, subject)
  end
  
  on class: :statement,feeling: :positive, include: [:subject] do |subject|
    ChatResponses.get(:positive_response)
  end

  on class: :statement, feeling: :negative, include: [:subject] do |subject|
    ChatResponses.get(:negative_response)
  end

  on regex: /(can|is|are|could)\s(.*)\s(.*)\?/ do |regex|
    a = regex[1]
    b = regex[2]

    if (@context[b] != nil && (@context[b].include? a))
      ChatResponses.get(:boolean_yes)
    else
      ChatResponses.get(:boolean_no)
    end
  end
   
  # Capture anything not recognized as a question 
  on regex: /\?/, include: [:subject] do |subject|
   ChatResponses.get(:boolean_answer,subject) 
  end

  on regex: /wow.*!/ , class: :positive do
    ChatResponses.get(:positive_response)
  end

  on regex: /wow.*!/, class: :negative do
    ChatResponses.get(:negative_response)
  end
 
  on regex: /no,/ do
    ChatResponses.get(:explanation)
  end

  on regex: /yes,/ do
    ChatResponses.get(:explanation)
  end
  
  on class: [:greeting, :positive] do
    ["Hello!", "Nice to see you again", "What's up!", "Howdy"].sample
  end

  on class: [:greeting, :negative] do
    ["That's not a very nice way to greet somebody", "Hello.. I guess", "I thought you wanted to talk to me"].sample
  end
    
  on class: :greeting do
    ["Hello", "How is it going?"].sample
  end

 
end
