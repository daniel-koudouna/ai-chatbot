require_relative './chatbot/chatbot.rb'

chat_bot = ChatBot.new "Chatting module"


puts "TYPE 'exit' TO EXIT THE CONVERSATION"

puts "> #{chat_bot.respond_to 'Hello'}"

while true
  input = gets
  if (input != "exit\n")
    puts "> #{chat_bot.respond_to input.chomp}"
  else
    chat_bot.save
    break
  end
end
