require 'open-uri'

URL = "http://wordnetweb.princeton.edu/perl/webwn?s="

LIMIT = 5000

lines = File.foreach('words.txt').first(LIMIT)

words = {}

puts ""

lines.each_with_index do |line, n|
  word = line.split(" ")[0].downcase.strip
  print "\rReading #{word}                                                   #{100*(n*1.0/LIMIT*1.0)}%                          "
  words[word] = []
  open(URL + word) do |f|
    content = f.read
    matches = content.scan /<li>(.*)<\/li>/
    next if matches == nil
    matches.flatten.each do |listing|
      meanings = listing.scan /<a href=.*>(.*)<\/a>/
      meanings.flatten.each do |meaning|
        next if meaning.match /\(.*\)/
        words[word] << meaning.strip.downcase
      end
    end
  end
end


puts ""

File.open("words.dat", "w") {|f| f.write(Marshal.dump(words)) }
